# Apps Collection [2020] 

Created smaller Apps [Followed Tutorials].

## Dependencies
- [audioplayers](https://pub.dev/packages/audioplayers)
- [rflutter_alert](https://pub.dev/packages/rflutter_alert)
- [font_awesome_flutter](https://pub.dev/packages/font_awesome_flutter)

## App Overview

### Overview Screen

[![Overview Screen](/images/readme/overview.png "Overview Screen")

### I am Rich Screen

[![I am Rich Screen](/images/readme/i_am_rich.png "I am Rich Screen")

### I am Poor Screen

[![I am Poor Screen](/images/readme/i_am_poor.png "I am Poor Screen")

### My Card Screen

[![My Card Screen](/images/readme/my_card.png "My Card Screen")

### Dicetor

[![Dicetor Screen](/images/readme/dicetor.png "Dicetor Screen")

### Magic 8 Ball Screen

[![Magic 8 Ball Screen](/images/readme/magic_8_ball.png "Magic 8 Ball Screen")

### Viktonius Screen

[![Viktonius Screen](/images/readme/viktonius.png "Viktonius Screen")

### Quizzler Screen

[![Quizzler Screen](/images/readme/quizzler.png "Quizzler Screen")

### Destini Screen

[![Destini Screen](/images/readme/destini.png "Destini Screen")

### BMI Calculator Screen

[![BMI Calculator Screen](/images/readme/bmi_calculator.png "BMI Calculator Screen")
