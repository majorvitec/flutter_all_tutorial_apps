import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  AppButton({
    required this.route,
    required this.title,
  });

  final String route;
  final String title;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, route);
      },
      child: Container(
        margin: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          border: Border.all(
            color: Color(0xFFEEEEEE),
            width: 1,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        child: Center(
          child: Container(child: Text(title)),
        ),
      ),
    );
  }
}
