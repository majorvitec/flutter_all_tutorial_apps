import 'package:apps_collection/apps/viktonius/components/sound.dart';
import 'package:apps_collection/apps/viktonius/constants.dart';

class SoundData {
  final List soundNames = [
    'möp',
    'mi',
    'mo',
    'mu',
    'ma',
    'me',
    'müh',
    'li',
    'lo',
    'lu',
    'la',
    'le',
  ];
  List<Sound> _sounds = [];

  List<Sound> getSoundData() {
    _sounds = [];

    for (String name in soundNames) {
      Sound sound = Sound(
        name: name,
        path: '$kBaseSoundPath/' + name + '.wav',
        imgPath: '$kBaseImagePath/' + name + '_head.png',
      );
      _sounds.add(sound);
    }

    return _sounds;
  }
}
