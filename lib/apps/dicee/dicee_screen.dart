import 'dart:math';

import 'package:apps_collection/apps/dicee/components/dice.dart';
import 'package:apps_collection/apps/dicee/components/face_sheet.dart';
import 'package:apps_collection/apps/dicee/constants.dart';
import 'package:apps_collection/apps/dicee/utils/portrait_mode.dart';
import 'package:flutter/material.dart';

const _imgPath = 'images/dice';

class DiceeScreen extends StatelessWidget with PortraitModeMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context); // only portrait mode is possible
    return Scaffold(
      backgroundColor: kScaffoldBackgroundColor,
      appBar: AppBar(
        title: Center(
          child: Text(
            kAppBarTitle.toUpperCase(),
          ),
        ),
      ),
      body: DicePage(),
    );
  }
}

class DicePage extends StatefulWidget {
  @override
  _DicePageState createState() => _DicePageState();
}

class _DicePageState extends State<DicePage> {
  late int _leftDiceNumber;
  late int _rightDiceNumber;

  @override
  // ignore: must_call_super
  void initState() {
    super.initState();
    _leftDiceNumber = setRandomNumber(6);
    _rightDiceNumber = setRandomNumber(6);
  }

  int setRandomNumber(int range) {
    return Random().nextInt(range) + 1; // 1..6
  }

  void changeDiceNumbers() {
    setState(() {
      _leftDiceNumber = setRandomNumber(6);
      _rightDiceNumber = setRandomNumber(6);
    });
  }

  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            flex: 4,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Center(
                      child: Text(
                        kSheetTitle.toUpperCase(),
                        style: kSheetTitleStyle,
                      ),
                    ),
                  ),
                ),
                Divider(
                  color: kDividerColor,
                  thickness: kDividerThickness,
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      FaceSheet(
                        imgPath: '$_imgPath/dice1.png',
                        title: '1',
                      ),
                      FaceSheet(
                        imgPath: '$_imgPath/dice2.png',
                        title: '2',
                      ),
                      FaceSheet(
                        imgPath: '$_imgPath/dice3.png',
                        title: '3',
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    FaceSheet(
                      imgPath: '$_imgPath/dice4.png',
                      title: '4',
                    ),
                    FaceSheet(
                      imgPath: '$_imgPath/dice5.png',
                      title: '5',
                    ),
                    FaceSheet(
                      imgPath: '$_imgPath/dice6.png',
                      title: '6',
                    ),
                  ],
                ),
              ],
            ),
          ),
          Divider(
            color: kDividerColor,
            thickness: kDividerThickness,
          ),
          Expanded(
            flex: 4,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Dice(
                    onPressed: () {
                      changeDiceNumbers();
                    },
                    imgPath: '$_imgPath/dice$_leftDiceNumber.png',
                    diceNumber: _leftDiceNumber.toString(),
                  ),
                ),
                Expanded(
                  child: Dice(
                    onPressed: () {
                      changeDiceNumbers();
                    },
                    imgPath: '$_imgPath/dice$_rightDiceNumber.png',
                    diceNumber: _rightDiceNumber.toString(),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                border: Border(
                  top: kButtonBorderSide,
                  bottom: kButtonBorderSide,
                ),
              ),
              child: TextButton(
                onPressed: () {
                  changeDiceNumbers();
                },
                style: TextButton.styleFrom(backgroundColor: kButtonColor),
                child: Text(
                  'dice me'.toUpperCase(),
                  style: kButtonStyle,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
