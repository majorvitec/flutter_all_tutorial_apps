import 'package:apps_collection/apps/quizzler/components/question.dart';
import 'package:apps_collection/apps/quizzler/components/score_keeper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class QuizBrain {
  ScoreKeeper _scoreKeeper = ScoreKeeper();

  int _questionNumber = 0;
  bool _gameFinished = false;
  List<Question> _questions = [
    Question(
      question: 'Some cats are actually allergic to humans',
      answer: true,
    ),
    Question(
      question: 'You can lead a cow down stairs but not up stairs.',
      answer: false,
    ),
    Question(
      question: 'Approximately one quarter of human bones are in the feet.',
      answer: true,
    ),
    Question(
      question: 'A slug\'s blood is green.',
      answer: true,
    ),
    Question(
      question: 'Buzz Aldrin\'s mother\'s maiden name was \"Moon\".',
      answer: true,
    ),
    Question(
      question: 'It is illegal to pee in the Ocean in Portugal.',
      answer: true,
    ),
    Question(
      question:
          'No piece of square dry paper can be folded in half more than 7 times.',
      answer: false,
    ),
    Question(
      question:
          'In London, UK, if you happen to die in the House of Parliament, you are technically entitled to a state funeral, because the building is considered too sacred a place.',
      answer: true,
    ),
    Question(
      question:
          'The loudest sound produced by any animal is 188 decibels. That animal is the African Elephant.',
      answer: false,
    ),
    Question(
      question:
          'The total surface area of two human lungs is approximately 70 square metres.',
      answer: true,
    ),
    Question(
      question: 'Google was originally called \"Backrub\".',
      answer: true,
    ),
    Question(
      question:
          'Chocolate affects a dog\'s heart and nervous system; a few ounces are enough to kill a small dog.',
      answer: true,
    ),
    Question(
      question:
          'In West Virginia, USA, if you accidentally hit an animal with your car, you are free to take it home to eat.',
      answer: true,
    ),
  ];

  String _currentQuestion = '';

  void _checkQuestionAnswer(bool answer) {
    if (answer == getAnswer()) {
      _scoreKeeper.addTrueToScore();
    } else {
      _scoreKeeper.addFalseToScore();
    }
  }

  void showAlert(BuildContext context) {
    Alert(
      context: context,
      type: AlertType.success,
      title: 'Game Finished',
      desc: 'Congratulation you have finished the game.',
      buttons: [
        DialogButton(
          onPressed: () => Navigator.pop(context),
          child: Text(
            'Restart Game',
            style: TextStyle(fontSize: 20),
          ),
          width: 160,
        ),
      ],
    ).show();
  }

  void resetGame(BuildContext context) {
    showAlert(context);
    _questionNumber = 0;
    _scoreKeeper.resetScore();
    _gameFinished = false;
  }

  void getNextQuestion(BuildContext context, bool answer) {
    if (_gameFinished) {
      resetGame(context);
      return;
    }
    _checkQuestionAnswer(answer);

    if (_questionNumber < _questions.length - 1) {
      _questionNumber++;
    }

    if (_questionNumber == _questions.length - 1) {
      _checkQuestionAnswer(answer);
      _gameFinished = true;
    }
  }

  String getQuestion() {
    return _questions[_questionNumber].question;
  }

  String getCurrentQuestion() {
    if (_currentQuestion == '') {
      _currentQuestion = _questions[_questionNumber].question;
    }
    return _currentQuestion;
  }

  bool getAnswer() {
    return _questions[_questionNumber].answer;
  }

  List<Widget> getScore() {
    return _scoreKeeper.getScore();
  }
}
