import 'package:apps_collection/apps/bmi_calculator/components/page_button.dart';
import 'package:apps_collection/apps/bmi_calculator/components/resuable_card.dart';
import 'package:apps_collection/apps/bmi_calculator/constants.dart';
import 'package:flutter/material.dart';

class CalculationPage extends StatelessWidget {
  CalculationPage({
    required this.bmiResult,
    required this.resultText,
    required this.resultInterpretation,
  });

  final String resultText;
  final String bmiResult;
  final String resultInterpretation;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Center(
          child: Text(
            'bmi calculator'.toUpperCase(),
          ),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.all(15.0),
              alignment: Alignment.bottomLeft,
              child: Text(
                'your result'.toUpperCase(),
                style: kResultTitleStyle,
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: ReusableCard(
              cardColor: kActiveCardColor,
              cardChild: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      resultText.toUpperCase(),
                      style: kResultTextStyle,
                    ),
                    Text(
                      bmiResult,
                      style: kResultNumberStyle,
                    ),
                    Text(
                      resultInterpretation,
                      style: kResultDescriptionStyle,
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          ),
          PageButton(
            onTap: () {
              Navigator.pop(context);
            },
            buttonTitle: 're-calculate'.toUpperCase(),
          ),
        ],
      ),
    );
  }
}
